#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import Optional

import requests
import yaml


OO_API_URL = "https://developer.overheid.nl/oo/api/v0"
OO_API_CACHE = {}


def get_organization(id: int) -> requests.Response:
    if id not in OO_API_CACHE:
        OO_API_CACHE[id] = requests.get(f"{OO_API_URL}/organisaties/{id}")

    return OO_API_CACHE[id]


def validate_file(file: Path) -> Optional[str]:
    with file.open() as f:
        instance = yaml.safe_load(f)

    if "organization" not in instance or "ooid" not in instance["organization"]:
        return "Missing organization ID"

    response = get_organization(instance["organization"]["ooid"])

    if not response.ok:
        return f"{response.status_code} {response.reason}"

    organization = response.json()
    organization_name = organization["naam"]

    existing_name = instance["organization"]["name"]

    if existing_name != organization_name:
        return f"Name does not match, got: '{existing_name}', should be: '{organization_name}'"

    return None

def main(path: Path) -> int:
    if path.is_file():
        files = [path]
    elif path.is_dir():
        files = sorted(path.glob("*.yaml"))

    has_failures = False

    for file in files:
        error = validate_file(file)
        if error is not None:
            has_failures = True
            print(f"{file}: {error}")

    return 1 if has_failures else 0


def parse_aruments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Validate the organization in a file")

    parser.add_argument("path", help="Directory or file to validate")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_aruments()
    exit(main(Path(args.path)))
