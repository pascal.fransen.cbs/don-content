#!/usr/bin/env python3
import argparse
from locale import normalize
from pathlib import Path
from urllib.parse import urlparse

import yaml


def main(directory: Path) -> int:
    api_directory = directory / "api"

    has_failures = False
    duplicate_count = 0
    urls_seen = {}

    for file in sorted(api_directory.glob("*.yaml")):
        with file.open() as f:
            instance = yaml.safe_load(f)

        errors = {}

        for environment in instance["environments"]:
            name = environment["name"]
            api_url = urlparse(environment["api_url"])
            normalized_url = f"{api_url.netloc}{api_url.path}"

            if normalized_url in urls_seen:
                errors[name] = f"{api_url.scheme}://{normalized_url}"
                duplicate_count += 1
            else:
                urls_seen[normalized_url] = name

        if errors:
            has_failures = True
            print(f"{file}:")
            for environment, url in errors.items():
                print(f"  Found duplicate API URL: {environment}: {url}")

    if duplicate_count:
        print(f"Duplicate URLs found: {duplicate_count}")

    return 1 if has_failures else 0


def parse_aruments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Validate repository files")

    parser.add_argument("directory", help="Content directory to validate")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_aruments()
    exit(main(Path(args.directory)))
